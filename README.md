# Dealio #

Dealio is a PHP-Based Daily Deal Portal. It's used to build, administrate and manage a Daily Deals / Coupons Website.

### What's Needed ###

* PHP 5.6 or Later
* MySQL


### Current Version ###

* 1.0.1 - Beta